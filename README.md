# scala-test

To build the application:

    sbt clean test pack

Upon success, inside `target` there will be a `pack` sub-folder with the following content:

* bin -> includes `entrypoint.sh` which can be used to run the application.
* conf -> configuration file(s) (i.e. `application.conf`)
* lib -> generated JAR and it's dependencies

By default, the sever runs on port 8081, but this can be changed via the configuration file.

The default logging level is set to INFO, and only console output is used.
