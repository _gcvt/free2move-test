import com.typesafe.sbt.SbtScalariform
import com.typesafe.sbt.SbtScalariform.ScalariformKeys
import scalariform.formatter.preferences._
import ScalariformProps._
import xerial.sbt.Pack._

scalaVersion in ThisBuild := "2.12.2"

lazy val commonSettings = Seq(
  organization := "org.example",
  version := "0.1",
  scalaVersion := "2.12.2",
  parallelExecution in Test := false,
  scalacOptions := Seq(
    "-deprecation",
    "-explaintypes",
    "-feature",
    "-language:postfixOps",
    "-language:implicitConversions",
    "-target:jvm-1.8",
    "-unchecked",
    "-Xcheckinit",
    "-Xfuture",
    "-Xlint",
    "-Xfatal-warnings",
    "-Xverify",
    "-Ywarn-adapted-args",
    "-Ywarn-dead-code",
    "-Ywarn-inaccessible",
    "-Ywarn-infer-any",
    "-Ywarn-nullary-override",
    "-Ywarn-nullary-unit",
    "-Ywarn-unused",
    "-Ywarn-unused-import"
  )
)

lazy val scalariformSettings = SbtScalariform.scalariformSettings ++ Seq(
  ScalariformKeys.preferences := setupScalariform(file("."), ScalariformKeys.preferences.value)
)

lazy val packSettings = packAutoSettings ++ Seq(
  packGenerateWindowsBatFile := false,
  packResourceDir := Map(baseDirectory.value / "src" / "main" / "conf" -> "conf"),
  packJvmOpts := Map("entrypoint.sh" -> Seq("-Dconfig.file=${PROG_HOME}/conf/application.conf")),
  packMain := Map("entrypoint.sh" -> "org.example.free2move.Main"),
  packArchiveName := "scala-test"
)

lazy val `scala-test` = (project in file("."))
  .settings(commonSettings ++ scalariformSettings ++ packSettings)
  .settings(
    name := "scala-test",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % "10.0.6",
      "com.typesafe.akka" %% "akka-slf4j" % "2.4.18",
      "ch.qos.logback" % "logback-classic" % "1.1.3",
      // testing
      "org.scalatest" %% "scalatest" % "3.0.0" % Test,
      "org.scalacheck" %% "scalacheck" % "1.13.4" % Test,
      "org.mockito" % "mockito-core" % "1.9.5" % Test,
      "com.typesafe.akka" %% "akka-http-testkit" % "10.0.6" % Test
    )
  )
