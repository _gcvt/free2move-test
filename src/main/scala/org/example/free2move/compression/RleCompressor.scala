package org.example.free2move.compression

import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

class RleCompressor extends Compressor {
  // this implementation could be optimized, if we knew that only one element at a time is added
  // (this actually is the case, but the Compressor interface does not enforce it)
  override def compress[A](as: Seq[A], existing: Seq[Compressed[A]]): Seq[Compressed[A]] =
    as.foldLeft(existing.toList.reverse)(prepend).reverse

  override def decompress[A](as: Seq[Compressed[A]], nElements: Int): Seq[A] = {
    @tailrec
    def loop(as: Seq[Compressed[A]], n: Int, agg: ListBuffer[A]): Seq[A] = {
      if (as.isEmpty || n <= 0) agg.toList
      else as.head match {
        case Single(a)        => loop(as.tail, n - 1, agg += a)
        case Repeat(count, a) => loop(as.tail, n - count, agg ++= List.fill(count)(a))
      }
    }
    loop(as, nElements, ListBuffer.empty[A])
  }

  private def prepend[A](existing: List[Compressed[A]], a: A): List[Compressed[A]] = existing match {
    case Single(`a`) :: tail    => Repeat(2, a) :: tail
    case Repeat(n, `a`) :: tail => Repeat(n + 1, a) :: tail
    case _                      => Single(a) :: existing
  }
}
