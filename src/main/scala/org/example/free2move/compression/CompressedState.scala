package org.example.free2move.compression

case class CompressedState[A](content: Seq[Compressed[A]])(implicit compressor: Compressor) {
  def append(as: A*): CompressedState[A] =
    CompressedState(compressor.compress(as.toSeq, content))

  def decompressed(nElements: Int = Int.MaxValue): Seq[A] =
    compressor.decompress(content, nElements)
}

object CompressedState {
  def empty[A](implicit compressor: Compressor): CompressedState[A] = CompressedState[A](Seq.empty)
}