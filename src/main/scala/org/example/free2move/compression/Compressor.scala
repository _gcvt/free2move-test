package org.example.free2move.compression

/**
 * The method signatures have been modified to better support the goal of the application.
 */
trait Compressor {
  /** compress input sequence, optionally adding it to the existing compressed sequence */
  def compress[A](as: Seq[A], existing: Seq[Compressed[A]] = Seq.empty): Seq[Compressed[A]]

  /**
   * decompress input sequence, optionally up to the given index. note that the resulting
   * sequence may have more elements than requested e.g.
   * if nElements == 1 and as = Seq(Repeat(5, "s")), the result will still include 5 elements
   */
  def decompress[A](as: Seq[Compressed[A]], nElements: Int = Int.MaxValue): Seq[A]
}
