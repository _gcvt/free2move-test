package org.example.free2move

import com.typesafe.config.ConfigFactory

import scala.concurrent.duration.{ Duration, FiniteDuration }

class Config {
  private val cfg = ConfigFactory.load()

  val FetchUrl: String = cfg.getString("free2move.fetch-url")
  val FetchInterval: FiniteDuration = Duration.fromNanos(cfg.getDuration("free2move.fetch-interval").toNanos)

  val ServerPort: Int = cfg.getInt("free2move.server-port")
}
