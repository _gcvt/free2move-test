package org.example.free2move

import akka.actor.{ ActorSystem, Props }
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.util.Timeout
import org.example.free2move.actors.Receiver.Ping
import org.example.free2move.actors.{ Cache, Receiver }
import org.example.free2move.compression.{ Compressor, RleCompressor }

import scala.concurrent.duration._
import scala.util.{ Failure, Success }

object Main extends App {
  implicit val actorSystem = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val ec = scala.concurrent.ExecutionContext.Implicits.global
  implicit val timeout = Timeout(5 seconds)

  implicit val compressor: Compressor = new RleCompressor()
  val config = new Config()
  val http = Http(actorSystem)

  val cache = actorSystem.actorOf(Props[Cache], "cache")
  val receiver = actorSystem.actorOf(Receiver.props(config, http, cache), "receiver")

  actorSystem.scheduler.schedule(config.FetchInterval, config.FetchInterval, receiver, Ping())

  val bindingFuture = new Server(cache).start(config.ServerPort)
  bindingFuture.onComplete {
    case Success(_) =>
      actorSystem.log.info(s"Server listening on port ${config.ServerPort}")
    case Failure(ex) =>
      actorSystem.log.error("Server could not be started.", ex)
      actorSystem.terminate()
  }

  Runtime.getRuntime.addShutdownHook(new Thread(() =>
    bindingFuture.flatMap(_.unbind()).onComplete(_ => actorSystem.terminate())))
}
