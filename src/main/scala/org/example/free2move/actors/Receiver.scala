package org.example.free2move.actors

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import akka.http.scaladsl.HttpExt
import akka.http.scaladsl.model.{ HttpEntity, HttpRequest, HttpResponse, StatusCodes }
import akka.pattern.pipe
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Framing
import akka.util.ByteString
import org.example.free2move.Config
import org.example.free2move.actors.Cache.UpdateState
import org.example.free2move.compression.{ CompressedState, Compressor }

class Receiver(
    config: Config,
    http:   HttpExt,
    cache:  ActorRef
)(implicit compressor: Compressor, materializer: ActorMaterializer) extends Actor with ActorLogging {
  import Receiver._
  import context.dispatcher

  override def receive: Receive = {
    case Ping() =>
      log.info("Receiver pinged, fetching remote content...")
      http.singleRequest(HttpRequest(uri = config.FetchUrl)).pipeTo(self)
    case HttpResponse(StatusCodes.OK, _, entity, _) =>
      log.info("Receiver fetched remote content successfully, parsing response...")
      parseResponse(entity)
    case resp @ HttpResponse(code, _, _, _) =>
      log.warning(s"Receiver failed to fetch remote content. Response code: $code.")
      resp.discardEntityBytes()
    case _ =>
  }

  private def parseResponse(entity: HttpEntity): Unit = {
    entity.dataBytes
      .via(Framing.delimiter(ByteString("\n"), maximumFrameLength = 256, allowTruncation = true))
      .runFold(CompressedState.empty[String]) {
        case (state, elem) =>
          state.append(elem.decodeString("UTF-8"))
      }
      .map(UpdateState)
      .pipeTo(cache)
  }
}

object Receiver {
  case class Ping()

  def props(
    config: Config,
    http:   HttpExt,
    cache:  ActorRef
  )(implicit compressor: Compressor, materializer: ActorMaterializer): Props =
    Props(new Receiver(config, http, cache))
}