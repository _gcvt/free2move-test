package org.example.free2move.actors

import akka.actor.{ Actor, ActorLogging, Props }
import org.example.free2move.compression.CompressedState

class Cache extends Actor with ActorLogging {
  import Cache._
  private var state: Option[CompressedState[_]] = None

  override def receive: Receive = {
    case UpdateState(newState) =>
      log.info(s"Cache updating its state, now has ${newState.content.size} elements.")
      state = Some(newState)
    case GetByIndex(n) =>
      log.info(s"Cache returning state element by index $n...")
      sender() ! StateElementByIndex {
        if (n < 0) None
        else state.flatMap(_.decompressed(n + 1).drop(n).headOption)
      }
  }
}

object Cache {
  case class UpdateState(state: CompressedState[_])
  case class GetByIndex(n: Int)
  case class StateElementByIndex(value: Option[_])

  def props: Props = Props(new Cache())
}
