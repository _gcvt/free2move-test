package org.example.free2move

import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import org.example.free2move.actors.Cache.{ GetByIndex, StateElementByIndex }

import scala.concurrent.{ ExecutionContext, Future }

class Server(
    cache: ActorRef
)(implicit
  actorSystem: ActorSystem,
  materializer: ActorMaterializer,
  timeout:      Timeout,
  ec:           ExecutionContext) {

  val route = path(IntNumber) { index =>
    actorSystem.log.info(s"Server to return element with index $index")
    get {
      rejectEmptyResponse(complete {
        (cache ? GetByIndex(index)).mapTo[StateElementByIndex].map { stateElementByIndex =>
          stateElementByIndex.value.map(_.toString)
        }
      })
    }
  }

  def start(port: Int): Future[Http.ServerBinding] = {
    Http().bindAndHandle(route, "localhost", port)
  }
}
