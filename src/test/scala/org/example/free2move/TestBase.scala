package org.example.free2move

import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{ BeforeAndAfterAll, FlatSpecLike, Matchers }

trait TestBase extends FlatSpecLike with Matchers with GeneratorDrivenPropertyChecks with RemoteContentGen {
  def testIndices(lines: List[String]): Set[Int] = Set(-1, 0, lines.size / 2, lines.size - 1, lines.size)
}

abstract class TestBaseActors extends TestKit(ActorSystem("test-actor-system")) with TestBase with BeforeAndAfterAll {
  override def afterAll(): Unit = system.terminate()
}
