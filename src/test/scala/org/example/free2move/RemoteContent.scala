package org.example.free2move

import org.scalacheck.{ Arbitrary, Gen }

case class Block(lines: List[String])

case class RemoteContent(blocks: List[Block]) {
  val toList: List[String] = blocks.flatMap(_.lines)
}

trait RemoteContentGen {
  private lazy val blockGen: Gen[Block] = for {
    elem <- Gen.alphaUpperChar.map(_.toString)
    n <- Gen.choose(1, 10)
    lines <- Gen.listOfN(n, elem)
  } yield Block(lines)

  lazy val remoteContentGen: Gen[RemoteContent] = Gen.listOf(blockGen).map(RemoteContent)

  implicit lazy val remoteContentArb: Arbitrary[RemoteContent] = Arbitrary(remoteContentGen)
}
