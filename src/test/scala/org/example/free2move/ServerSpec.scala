package org.example.free2move

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ HttpRequest, StatusCodes }
import akka.stream.ActorMaterializer
import akka.util.Timeout
import org.example.free2move.actors.Cache
import org.example.free2move.actors.Cache.UpdateState
import org.example.free2move.compression.{ CompressedState, RleCompressor }

import scala.concurrent.duration._
import scala.concurrent.{ Await, Awaitable }

class ServerSpec extends TestBaseActors {
  implicit val compressor = new RleCompressor()
  implicit val materializer = ActorMaterializer()
  val awaitDuration = 5 seconds
  implicit val timeout = Timeout(awaitDuration)
  implicit val ec = scala.concurrent.ExecutionContext.Implicits.global
  val port = 8083
  val cache = system.actorOf(Cache.props)

  behavior of classOf[Server].getSimpleName

  it should "return correct response on GET requests" in forAll { content: RemoteContent =>
    val lines = content.toList
    cache ! UpdateState(CompressedState.empty[String].append(lines: _*))

    val server = new Server(cache)
    val binding = await(server.start(port))
    try testGetRequests(lines)
    finally await(binding.unbind())
  }

  private def testGetRequests(lines: List[String]): Unit = {
    val http = Http()
    testIndices(lines).foreach { indexToRequest =>
      val resp = await(http.singleRequest(HttpRequest(uri = s"http://localhost:$port/$indexToRequest")))

      if (indexToRequest < 0 || indexToRequest >= lines.size) resp.status shouldBe StatusCodes.NotFound
      else {
        resp.status shouldBe StatusCodes.OK
        val content = await(resp.entity.toStrict(awaitDuration)).data.decodeString("UTF-8")
        content shouldBe lines(indexToRequest)
      }
    }
  }

  private def await[T](awaitable: Awaitable[T]): T = Await.result(awaitable, awaitDuration)
}
