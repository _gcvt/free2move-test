package org.example.free2move.compression

import org.example.free2move.{ RemoteContent, TestBase }

class RleCompressorSpec extends TestBase {

  behavior of classOf[RleCompressor].getSimpleName

  it should "compress and decompress string sequences" in forAll { content: RemoteContent =>
    val lines = content.toList
    val rleCompressor = new RleCompressor()
    val compressed = rleCompressor.compress(lines)

    val decompressed = rleCompressor.decompress(compressed)
    decompressed should contain theSameElementsInOrderAs lines
  }

  it should "support incremental compression" in forAll { content: RemoteContent =>
    val lines = content.toList
    val rleCompressor = new RleCompressor()
    val compressed = lines.foldLeft(Seq.empty[Compressed[String]]) {
      case (agg, line) => rleCompressor.compress(Seq(line), agg)
    }

    val decompressed = rleCompressor.decompress(compressed)
    decompressed should contain theSameElementsInOrderAs lines
  }

  it should "support decompression up to a certain element" in forAll { content: RemoteContent =>
    whenever(content.blocks.size > 1) {
      val lines = content.toList
      val rleCompressor = new RleCompressor()
      val compressed = rleCompressor.compress(lines)

      val nElements = content.blocks.head.lines.size
      val decompressed = rleCompressor.decompress(compressed, nElements)

      lines.mkString should startWith(decompressed.mkString)
      lines.size shouldBe >(decompressed.size)
    }
  }

}
