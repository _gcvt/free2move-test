package org.example.free2move.compression

import org.example.free2move.{ RemoteContent, TestBase }

class CompressedStateSpec extends TestBase {
  implicit val compressor = new RleCompressor()

  behavior of classOf[CompressedState[String]].getSimpleName

  it should "accumulate string sequence in a compressed form" in forAll { content: RemoteContent =>
    val lines = content.toList
    val state = lines.foldLeft(CompressedState.empty[String])(_.append(_))

    state.content should contain theSameElementsInOrderAs compressor.compress(lines)
    state.decompressed() should contain theSameElementsInOrderAs lines
  }

}
