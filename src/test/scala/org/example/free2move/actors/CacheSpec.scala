package org.example.free2move.actors

import akka.testkit.TestProbe
import org.example.free2move.compression.{ CompressedState, RleCompressor }
import org.example.free2move.{ RemoteContent, TestBaseActors }
import org.scalatest.Inside

import scala.concurrent.duration._

class CacheSpec extends TestBaseActors with Inside {
  import Cache._
  implicit val compressor = new RleCompressor()

  behavior of classOf[Cache].getSimpleName

  it should "keep the state and return its elements by index" in forAll { content: RemoteContent =>
    val lines = content.toList
    testIndices(lines).foreach { indexToRequest =>
      val cache = system.actorOf(props)
      val testProbe = TestProbe()

      val state = CompressedState.empty[String].append(lines: _*)
      cache ! UpdateState(state)

      testProbe.send(cache, GetByIndex(indexToRequest))

      inside(testProbe.receiveOne(5 seconds)) {
        case StateElementByIndex(e) =>
          if (indexToRequest >= 0 && indexToRequest < lines.size) e shouldBe Some(lines(indexToRequest))
          else e should not be 'defined
      }
    }
  }
}
