package org.example.free2move.actors

import akka.event.LoggingAdapter
import akka.http.scaladsl.model.{ HttpEntity, HttpRequest, HttpResponse }
import akka.http.scaladsl.settings.ConnectionPoolSettings
import akka.http.scaladsl.{ HttpExt, HttpsConnectionContext }
import akka.stream.{ ActorMaterializer, ActorMaterializerSettings, Materializer }
import akka.testkit.TestProbe
import org.example.free2move.actors.Cache.UpdateState
import org.example.free2move.compression.{ CompressedState, RleCompressor }
import org.example.free2move.{ Config, RemoteContent, TestBaseActors }
import org.mockito.Matchers.any
import org.mockito.Mockito.when
import org.scalatest.Inside
import org.scalatest.mockito.MockitoSugar

import scala.concurrent.Future
import scala.concurrent.duration._

class ReceiverSpec extends TestBaseActors with MockitoSugar with Inside {
  implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system))
  implicit val compressor = new RleCompressor()
  val config = new Config()

  behavior of classOf[Receiver].getSimpleName

  it should "fetch remote content and forward it to the cache actor" in forAll { content: RemoteContent =>
    val lines = content.toList
    val cacheProbe = TestProbe()

    val http = mock[HttpExt]
    val httpResponse = HttpResponse(entity = HttpEntity(lines.mkString("\n")))

    val actor = system.actorOf(Receiver.props(config, mockHttp(http, httpResponse), cacheProbe.ref))
    actor ! Receiver.Ping()

    inside(cacheProbe.receiveOne(5 seconds)) {
      case UpdateState(state @ CompressedState(_)) =>
        val received = state.decompressed().map(_.asInstanceOf[String])
        received should contain theSameElementsInOrderAs lines
    }
  }

  private def mockHttp(http: HttpExt, response: HttpResponse): HttpExt = {
    when(
      http.singleRequest(
        any[HttpRequest],
        any[HttpsConnectionContext],
        any[ConnectionPoolSettings],
        any[LoggingAdapter]
      )(any[Materializer])
    ).thenReturn(Future.successful(response))
    http
  }
}
