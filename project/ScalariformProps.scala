import java.io.{ FileInputStream, File }
import java.util.Properties

import scala.io.Source
import scalariform.formatter.preferences._

object ScalariformProps {
  def setupScalariform(baseDir: File, inputPreferences: IFormattingPreferences): IFormattingPreferences = {
    val properties = PreferencesImporterExporter.asProperties(inputPreferences)
    val customProperties = loadProperties(baseDir)

    properties.putAll(customProperties)

    PreferencesImporterExporter.getPreferences(properties)
  }

  private def loadProperties(baseDir: File): Properties = {
    val properties = new Properties()
    val in = new FileInputStream(new File(baseDir, "scalariform.properties"))
    try properties.load(in)
    finally in.close()
    properties
  }
}